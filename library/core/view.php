<?php

class View {
    public $_layout = true;
    public $action;
    public $controller;

    public function __construct() {
        
    }

    public function render($controller, $action) {
        if($this->_layout === true && $controller === 'admin'){
            $this->controller = $controller;
            $this->action = $action;
            require_once ROOT . DS . 'application' . DS . 'views' . DS . 'admin' . DS . 'layout' . DS . 'layout.html';
        }elseif($this->_layout === false && $controller === 'admin'){
            require_once ROOT . DS . 'application' . DS . 'views' . DS . $controller . DS . $action . '.html';
        }elseif($this->_layout === true && ($controller === '' || $controller === 'index')){
            $this->controller = $controller;
            $this->action = $action;
            require_once ROOT . DS . 'application' . DS . 'views' . DS . 'index' . DS . 'layout' . DS . 'layout.html';
        }
        
        
        /*
         * elseif lerle devam edilecek. 
         * layout un true olup controller ın index olduğu durumlar
         * için elseif ler yazılacak
         */
    }
}

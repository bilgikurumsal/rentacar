<?php
spl_autoload_register(null, false);

function __autoload($class_name)
{
    $class_name = strtolower($class_name);
    $file = ROOT . DS . 'library' . DS . 'core' . DS . $class_name. '.php';
    if (file_exists($file))
    {
        require_once($file);
    }
}


spl_autoload_register('__autoload');
$registry = Registry::instance();
$model = new Model();
$model->query('SELECT * FROM site_ayarlari');
$data = $model->single();
$registry->set('site_ayarlari', $data);

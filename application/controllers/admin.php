<?php

class adminController extends Controller {
    
    public $session;
    
    public function __construct() {
        $this->session = Sessions::singleton();
        $this->_controller = basename(__FILE__, '.php');
        $this->_view = new View();
        $this->_model = new Model();
    }

    public function indexAction() {
        if ($this->session->__get('SESSIONDATA') === 'loggedin') {
            $this->_action = substr(__FUNCTION__, 0, -6);
            $this->_view->render($this->_controller, $this->_action);
        }else{
            header('Location: /admin/login');
        }
    }

    public function ayarlarAction() {
        if ($this->session->__get('SESSIONDATA') === 'loggedin') {
            $this->_action = substr(__FUNCTION__, 0, -6);
            
            $this->_model->query('SELECT * FROM site_ayarlari');
            $this->_view->data = $this->_model->single();
            
            $this->_model->query('SELECT id,tema_adi FROM temalar');
            $this->_view->temalar = $this->_model->resultSet();
            
            if(isset($_POST['submit_form'])){
                $arrPost = $this->handlePost();
                //$tema = $arrPost['inputEmail3'];
                //$arkaplan_resmi = md5($arrPost['inputPassword3']);
                $site_baslik = $arrPost['site_baslik'];
                $site_aciklama = $arrPost['site_aciklama'];
                $site_keywords = $arrPost['site_keywords'];
                $copyright_yazisi = $arrPost['copyright_yazisi'];
                $analytics_kodu = $arrPost['analytics_kodu'];
                //$logo = $arrPost['logo yolu'];
                $sag_alan = $arrPost['sag_alan'];
                $orta_alan = $arrPost['orta_alan'];
                $logo_yani = $arrPost['logo_yani'];
                $durum = $arrPost['optionsRadios'];
                
                $this->_model->query('UPDATE site_ayarlari SET tema = :tema,'
                                    . 'arkaplan_resmi = :arkaplan_resmi,'
                                    . 'site_baslik = :site_baslik,'
                                    . 'site_aciklama = :site_aciklama,'
                                    . 'site_keywords = :site_keywords,'
                                    . 'copyright_yazisi = :copyright_yazisi,'
                                    . 'analytics_kodu = :analytics_kodu,'
                                    . 'logo = :logo,'
                                    . 'sag_alan = :sag_alan,'
                                    . 'orta_alan = :orta_alan,'
                                    . 'logo_yani = :logo_yani,'
                                    . 'doviz_kuru = :durum');
                
                $this->_model->bind(':tema', 2);
                $this->_model->bind(':arkaplan_resmi', '/arka/plan.resmi');
                $this->_model->bind(':site_baslik', $site_baslik);
                $this->_model->bind(':site_aciklama', $site_aciklama);
                $this->_model->bind(':site_keywords', $site_keywords);
                $this->_model->bind(':copyright_yazisi', $copyright_yazisi);
                $this->_model->bind(':analytics_kodu', $analytics_kodu);
                $this->_model->bind(':logo', '/logo/yolu.png');
                $this->_model->bind(':sag_alan', $sag_alan);
                $this->_model->bind(':orta_alan', $orta_alan);
                $this->_model->bind(':logo_yani', $logo_yani);
                $this->_model->bind(':durum', $durum);
                
                //$this->_model->execute();
                $this->_model->single();
                header('Location: /admin/ayarlar');
            }
            
            
            $this->_view->render($this->_controller, $this->_action);
        }else{
            header('Location: /admin/login');
        }
    }

    public function araclarimizAction() {
        if ($this->session->__get('SESSIONDATA') === 'loggedin') {
            $this->_action = substr(__FUNCTION__, 0, -6);
            $this->_view->render($this->_controller, $this->_action);
        } else {
            header('Location: /admin/login');
        }
    }

    public function sssAction() {
        if ($this->session->__get('SESSIONDATA') === 'loggedin') {
            $this->_action = substr(__FUNCTION__, 0, -6);
            $this->_view->render($this->_controller, $this->_action);
        } else {
            header('Location: /admin/login');
        }
    }

    public function iletisimAction() {
        if ($this->session->__get('SESSIONDATA') === 'loggedin') {
            $this->_action = substr(__FUNCTION__, 0, -6);
            $this->_view->render($this->_controller, $this->_action);
        } else {
            header('Location: /admin/login');
        }
    }

    public function hakkimizdaAction() {
        if ($this->session->__get('SESSIONDATA') === 'loggedin') {
            $this->_action = substr(__FUNCTION__, 0, -6);
            $this->_view->render($this->_controller, $this->_action);
        } else {
            header('Location: /admin/login');
        }
    }

    public function loginAction() {
        if($this->session->__get('SESSIONDATA') === 'loggedin'){
            header( 'Location: /admin/ayarlar' );
        }else{
            $this->_view->_layout = false;

            /* $pstInst = new Router();
              $pstInst->reqPost(); */

            /* controller'dan login.html içindeki
             * $this->h ye değer yollandı ve orada kullanıldı
             */
            $this->_view->hg = "LOGIN";

            if(!empty($_POST)){
                $arrPost = $this->handlePost();
                $usernamePost = $arrPost['inputEmail3'];
                $passwordPost = md5($arrPost['inputPassword3']);
                
                //$model->query('SELECT FName, LName, Age, Gender FROM mytable WHERE FName = :fname');
                $this->_model->query('SELECT username,password FROM users WHERE username = :username AND password = :password');
                
                $this->_model->bind(':username', $usernamePost);
                $this->_model->bind(':password', $passwordPost);
                
                $row = $this->_model->single();
                //$row = $this->model->rowCount();
                
                if($usernamePost == $row['username'] && $passwordPost == $row['password']){
                    $this->session->__set('SESSIONDATA', 'loggedin');
                    header('Location: /admin/ayarlar');
                }else{
                    $this->model->error = "Hatalı kullanıcı adı ya da şifre";
                    echo $this->_model->error;
                }
            }
            /*
             * şu aşağıdaki iki işlemi controller
             * içerisinde yapabilecek bi yöntem oluşturmayı dene
             */
            $this->_action = substr(__FUNCTION__, 0, -6);
            $this->_view->render($this->_controller, $this->_action);
        }
    }

    public function logoutAction() { // logout'un view'ı yok
        if ($this->session->__get('SESSIONDATA') === 'loggedin') {
            $this->session->destroy();
            $this->session->__destruct();
            header('Location: /admin/login');
        } else {
            header('Location: /admin/login');
        }
    }

    public function kaydetAction() { // yapılan değişikliklerin kaydedilmesi için düzenlenecek. kaydet'in view'ı yok.
        echo "adminController kaydetAction";
    }

    public function handlePost() { //post datasını handle edecek fonksiyonlar
        if(isset($_POST)){
            return $this->_pstData = $_POST;
        }
    }

}

<?php

class Model {

    private   $stmt;
    private   $dbInstance;
    protected $dbName;
    protected $dbHost;
    protected $dbUsername;
    protected $dbPassword;
    protected $dbOptions = array('PDO::ATTR_PERSISTENT' => true,
                                 'PDO::ATTR_ERRMODE' => 'PDO::ERRMODE_EXCEPTION');
    protected $dbDsn;
    protected $error;

    // 'mysql:host=localhost;dbname=test', $user, $pass
    public function __construct() {
        try {
            /*/
             * config/conf.ini den db ayarlarını çek
             */
            
            $filePath = ROOT . DS . 'config' . DS . 'conf.ini';
            $ini_array = parse_ini_file($filePath, true);
            
            $this->dbName = $ini_array['DB_AYARLAR']['dbname'];
            $this->dbHost = $ini_array['DB_AYARLAR']['dbhost'];
            $this->dbUsername = $ini_array['DB_AYARLAR']['dbusername'];
            $this->dbPassword = $ini_array['DB_AYARLAR']['dbpassword'];
            $this->dbDsn = 'mysql:host=' . $this->dbHost . ';dbname=' . $this->dbName . ';charset=utf8';
            
            $this->dbInstance = new PDO($this->dbDsn, $this->dbUsername, $this->dbPassword, $this->dbOptions);
        } catch (PDOException $ex) {
            $this->error = $ex->getMessage();
        }
    }

    public function query($query) {
        $this->stmt = $this->dbInstance->prepare($query);
    }
    
    public function bind($param, $value, $type = null) {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }
    
    public function execute() {
        return $this->stmt->execute();
    }
    
    public function resultset() {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function single() {
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }
    
    public function rowCount() {
        return $this->stmt->rowCount();
    }
    
    public function lastInsertId() {
        return $this->dbh->lastInsertId();
    }
    
    public function beginTransaction() {
        return $this->dbh->beginTransaction();
    }
    
    public function endTransaction() {
        return $this->dbh->commit();
    }

    public function cancelTransaction() {
        return $this->dbh->rollBack();
    }

    public function debugDumpParams() {
        return $this->stmt->debugDumpParams();
    }

}

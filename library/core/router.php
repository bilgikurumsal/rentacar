<?php

class Router {

    private $url; //get url'ler buradan ele alınacak
    private $className;
    private $class;
    private $_controller;
    private $_action;
    
    private static $instance; //singleton
    
    
    /*
     * url[0] ve url[1] için route'lar belirleniyor.
     * url[2] mevcut ise o parametre olarak alınacak.
     * url[3] mevcut ise parametre olarak alınacak
     * url[4] kabul edilmeyecek
     */
    private static $routes = array('admin' => array('' => 'index', // admin sayfası ve altındaki sayfalar için route'lar
                                                    'hakkimizda-hakkkk' => 'hakkimizda',
                                                    'site-ayarlari' => 'ayarlar',
                                                    'sikca-sorulan-sorular' => 'sss',
                                                    'bize-yazin' => 'iletisim'),
                                   '' => 'index', // public sayfalar için route'lar
                                   'index' => 'index',
                                   'arac-listele' => 'listele'); // index route aliasları aynı şekilde yaz

    public static function instance() {
        if (self::$instance == null) {
            self::$instance = new self;
        }
        return self::$instance;
    }
    
    public function __clone() {}

    /*public function init(){
        if (isset($_GET['url']) && is_string($_GET['url']) && preg_match('/^[a-zA-Z-\/]+$/i', $_GET['url'])) {
            $this->url = strtolower($_GET['url']);
            $this->url = rtrim($this->url, "/");
            $this->url = explode("/", $this->url);
            $file = ROOT . DS . 'application'. DS . 'controllers' . DS . $this->url[0] . '.php';
            if (file_exists($file)) {
                require_once($file);
                $className = $this->url[0] . 'Controller';
                $cont = new $className();
                if(isset($this->url[1])){
                    if(method_exists($cont, $this->url[1] . 'Action')){
                        $actionName = $this->url[1] . "Action";
                    }else{
                        $actionName = "indexAction";
                    }
                }
                else{
                    $actionName = "indexAction";
                }
                //var_dump($cont);exit();
                $cont->{$actionName}();
            } else {
                require_once(ROOT . DS . 'application' . DS . 'controllers' . DS . 'index.php');
                $cont = new indexController();
                $cont->indexAction();
            }
        } else {
            require_once(ROOT . DS . 'application' . DS . 'controllers' . DS . 'index.php');
            $cont = new indexController();
            $cont->indexAction();
        }
    }*/
    
    public function init(){
        if (isset($_GET['url']) && is_string($_GET['url']) && preg_match('/^[a-zA-Z-0-9\/]+$/i', $_GET['url'])) {
            $this->url = strtolower($_GET['url']);
            $this->url = rtrim($this->url, "/");
            $this->url = explode("/", $this->url);
            switch ($this->url[0]){
                case 'admin':
                    $file = ROOT . DS . 'application'. DS . 'controllers' . DS . $this->url[0] . '.php';
                    include_once $file;
                    $this->_controller = $this->url[0] . 'Controller';
                    $this->class = new $this->_controller();
                    switch ($this->url[1]){
                        case 'index':
                        case '':
                            $this->_action = 'indexAction';
                            $this->class->{$this->_action}();
                            break;
                        case 'ayarlar':
                        case 'site-ayarlari':
                            $this->_action = 'ayarlarAction';
                            $this->class->{$this->_action}();
                            break;
                        case 'araclarimiz':
                            $this->_action = $this->url[1] . 'Action';
                            $this->class->{$this->_action}();
                            break;
                        case 'hakkimizda':
                        case 'biz-kimiz':
                            $this->_action = 'hakkimizdaAction';
                            $this->class->{$this->_action}();
                            break;
                        case 'iletisim':
                            $this->_action = $this->url[1] . 'Action';
                            $this->class->{$this->_action}();
                            break;
                        case 'sss':
                            $this->_action = $this->url[1] . 'Action';
                            $this->class->{$this->_action}();
                            break;
                        case 'login':
                            $this->_action = $this->url[1] . 'Action';
                            $this->class->{$this->_action}();
                            break;
                        case 'logout':
                            $this->_action = $this->url[1] . 'Action';
                            $this->class->{$this->_action}();
                            break;
                        default :
                            header('Location: /');
                            break;
                    }
                    break;
                
                /*
                 * public altındaki diğer linkleri ekle.
                 * layoutlardan css ve javascriptlerin src yollarını düzgün alacak bi sistem
                 * yazmak gerekiyor. css lerde sorun çıkıyor
                 * ÖRN: http://localhost.mvc/index sorunsuzca yüklerken
                 *      http://localhost.mvc/index/ css i yüklemiyor.
                 */
                case 'index' : 
                case '':
                    $file = ROOT . DS . 'application'. DS . 'controllers' . DS . $this->url[0] . '.php';
                    include_once $file;
                    $this->_controller = $this->url[0] . 'Controller';
                    $this->_action = 'indexAction';
                    $this->class = new $this->_controller();
                    $this->class->{$this->_action}();
                    break;
                case 'hakkimizda':
                case 'biz-kimiz':
                    $file = ROOT . DS . 'application' . DS . 'controllers' . DS . 'index' . '.php';
                    include_once $file;
                    $this->_controller = 'index' . 'Controller';
                    $this->_action = 'hakkimizdaAction';
                    $this->class = new $this->_controller();
                    $this->class->{$this->_action}();
                    break;
                case 'araclarimiz':
                    $file = ROOT . DS . 'application' . DS . 'controllers' . DS . 'index' . '.php';
                    include_once $file;
                    $this->_controller = 'index' . 'Controller';
                    $this->_action = 'araclarimizAction';
                    $this->class = new $this->_controller();
                    $this->class->{$this->_action}(empty($this->url[1]) ? 1 : (int)$this->url[1]);
                    break;
                case 'iletisim':
                    $file = ROOT . DS . 'application' . DS . 'controllers' . DS . 'index' . '.php';
                    include_once $file;
                    $this->_controller = 'index' . 'Controller';
                    $this->_action = 'iletisimAction';
                    $this->class = new $this->_controller();
                    $this->class->{$this->_action}();
                    break;
                case 'sikca-sorulan-sorular':
                case 'sss':
                    $file = ROOT . DS . 'application' . DS . 'controllers' . DS . 'index' . '.php';
                    include_once $file;
                    $this->_controller = 'index' . 'Controller';
                    $this->_action = 'sssAction';
                    $this->class = new $this->_controller();
                    $this->class->{$this->_action}();
                    break;
                default :
                    header('Location: /');
                    break;
            }
        }else{
            $file = ROOT . DS . 'application' . DS . 'controllers' . DS . 'index' . '.php';
            include_once $file;
            $this->class = new indexController();
            $this->class->indexAction();
        }
        
    }
    
    public function setIni($assoc_arr, $path, $has_sections = FALSE) { // ini dosyasının içine yazmak için

        $content = "";
        if ($has_sections) {
            foreach ($assoc_arr as $key => $elem) {
                $content .= "[" . $key . "]\n";
                foreach ($elem as $key2 => $elem2) {
                    if (is_array($elem2)) {
                        for ($i = 0; $i < count($elem2); $i++) {
                            $content .= $key2 . "[] = \"" . $elem2[$i] . "\"\n";
                        }
                    } else if ($elem2 == "")
                        $content .= $key2 . " = \n";
                    else
                        $content .= $key2 . " = \"" . $elem2 . "\"\n";
                }
            }
        }
        else {
            foreach ($assoc_arr as $key => $elem) {
                if (is_array($elem)) {
                    for ($i = 0; $i < count($elem); $i++) {
                        $content .= $key2 . "[] = \"" . $elem[$i] . "\"\n";
                    }
                } else if ($elem == "")
                    $content .= $key2 . " = \n";
                else
                    $content .= $key2 . " = \"" . $elem . "\"\n";
            }
        }

        if (!$handle = fopen($path, 'w')) {
            return false;
        }
        if (!fwrite($handle, $content)) {
            return false;
        }
        fclose($handle);
        return true;
    }

    public function addRoute($controllerName, $actionName){
        /*
         * dosyaya route eklemek için
         */
    }
    
    public function removeRoute($controllerName, $actionName) {
        /*
         * dosyadan route çıkartmak için
         */
    }
    
}

<?php

class Registry {

    private static $instance;
    private $variables = array();

    public static function instance() {
        if (self::$instance == null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function __get($name) {
        // Check key exists
        if (!array_key_exists($name, $this->variables)) {
            return null;
        }

        return $this->variables[$name];
    }

    public function __set($name, $value) {
        // Simply store the value, overwriting any previous value
        $this->variables[$name] = $value;
    }

    public function __isset($name) {
        return isset($this->variables[$name]);
    }

    public function __unset($name) {
        unset($this->variables[$name]);
    }

    public static function get($name) {
        $instance = self::instance();
        return $instance->$name;
    }

    public static function set($name, $value) {
        $instance = self::instance();
        $instance->$name = $value;
    }

    public static function stored($name) {
        $instance = self::instance();
        return isset($instance->$name);
    }

    public static function remove($name) {
        $instance = self::instance();
        unset($instance->$name);
    }

}

<?php

abstract class Controller {

    protected $_controller;
    protected $_action;
    protected $_view;
    protected $_pstData;
    protected $_model;
    protected $_helper;

    abstract function indexAction();
    abstract function handlePost();
}

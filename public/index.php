<?php	
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));
define('DEVELOPMENT_ENVIRONMENT', FALSE);
define("PUBLIC_DIR", realpath(dirname(__FILE__)));

if (DEVELOPMENT_ENVIRONMENT == true) {
    error_reporting(E_ALL);
    ini_set('display_errors','On');
} else {
    error_reporting(E_ALL);
    ini_set('display_errors','Off');
    ini_set('log_errors', 'On');
    ini_set('error_log', ROOT.DS.'tmp'.DS.'logs'.DS.'error.log');
}

require_once (ROOT . DS . 'library' . DS . 'bootstrap.php');

$registry = Registry::instance();

$dbConf = array('DBNAME' => 'mvc',
                'DBUSERNAME' => 'root',
                'DBPASSWORD' => '',
                'DBHOST' => 'localhost');

$registry->set('DB', $dbConf);

$router = Router::instance();

$router->init();
<?php

class Helper {
    public $helper;
    public $output;
    public function __construct() {
        
    }

    public function _set($helperName) {
        $this->helper = $helperName;
        $file = ROOT . DS . 'application' . DS . 'helpers' . DS . $helperName . '.html';
        ob_start();
        include_once $file;
        $this->output = ob_get_contents();
        ob_end_clean();
    }
    public function _get(){
        return $this->output;
    }
}

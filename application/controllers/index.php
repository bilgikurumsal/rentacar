<?php

class indexController extends Controller {
    public $dataSayisi;
    public $toplamSayfa;
    public $sayfaSayisi;
    public static $limit = 10; // pagination limit
    public $offset;
    public $currentPage;

    public function __construct() {
        $this->_controller = basename(__FILE__, '.php');
        $this->_view = new View();
        $this->_helper = new Helper();
    }

    public function indexAction() {
        $this->_helper->_set('aracrezervasyon');
        $this->_view->helper->aracrezervasyon = $this->_helper->_get();
        
        $this->_action = substr(__FUNCTION__, 0, -6);
        //echo $this->_action . ' ' . $this->_controller;exit();
        $this->_view->render($this->_controller, $this->_action);
    }

    public function hakkimizdaAction() {
        $this->_helper->_set('aracrezervasyon');
        $this->_view->helper->aracrezervasyon = $this->_helper->_get();
        
        $this->_action = substr(__FUNCTION__, 0, -6);
        $this->_view->render($this->_controller, $this->_action);
    }
    
    public function araclarimizAction($page = 0) {
        $this->currentPage = $page;
        $this->_model = new Model();
        $this->_model->query('SELECT * FROM urunler');
        $this->_model->single();
        $this->toplamSayfa = $this->_model->rowCount();
        $this->sayfaSayisi = $this->toplamSayfa / self::$limit;
        $this->_view->sayfaSayisi = (int) $this->sayfaSayisi;
        
        if($this->currentPage == 1){
            $this->_view->currentPage = 1;
            $this->_model->query('SELECT * FROM urunler LIMIT :limit OFFSET :offset');
            $this->_model->bind(':limit', self::$limit, PDO::PARAM_INT);
            $this->_model->bind(':offset', 0, PDO::PARAM_INT);
            //$this->_model->single();
            $this->_view->data = $this->_model->resultSet();
            $this->dataSayisi = $this->_model->rowCount();
            $this->_view->dataSayisi = (int)$this->dataSayisi;
            
        }else{
            $this->_view->currentPage = $page;
            $this->offset = (self::$limit * ($page - 1));
            $this->_model->query('SELECT * FROM urunler LIMIT :limit OFFSET :offset');
            $this->_model->bind(':limit', self::$limit, PDO::PARAM_INT);
            $this->_model->bind(':offset', $this->offset, PDO::PARAM_INT);
            //$this->_model->single();
            $this->_view->data = $this->_model->resultSet();
            $this->dataSayisi = $this->_model->rowCount();
            $this->_view->dataSayisi = (int)$this->dataSayisi;
        }
        
        /*$this->_model->query('SELECT * FROM urunler LIMIT :limit OFFSET :offset');
        $this->_model->bind(':limit', 10, PDO::PARAM_INT);
        $this->_model->bind(':offset', 0, PDO::PARAM_INT);
        $this->_model->execute();
        $a = $this->_model->resultSet();        var_dump($a); exit();*/
        
        $this->_helper->_set('aracrezervasyon');
        $this->_view->helper->aracrezervasyon = $this->_helper->_get();
        
        $this->_action = substr(__FUNCTION__, 0, -6);
        $this->_view->render($this->_controller, $this->_action);
    }
    
    public function iletisimAction() {
        $this->_helper->_set('aracrezervasyon');
        $this->_view->helper->aracrezervasyon = $this->_helper->_get();
        
        $this->_action = substr(__FUNCTION__, 0, -6);
        $this->_view->render($this->_controller, $this->_action);
    }
    
    public function sssAction() {
        $this->_helper->_set('aracrezervasyon');
        $this->_view->helper->aracrezervasyon = $this->_helper->_get();
        
        $this->_action = substr(__FUNCTION__, 0, -6);
        $this->_view->render($this->_controller, $this->_action);
    }
    
    public function handlePost() {
        
    }

}

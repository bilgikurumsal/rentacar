<?php

class Sessions {

    private static $instance;
    public static $sessionID;

    private function __construct() {
        session_start();
        session_regenerate_id();
        self::$sessionID = session_id();
    }

    public static function singleton() {
        if (!isset(self::$instance)) {
            $className = __CLASS__;
            self::$instance = new $className;
        }

        return self::$instance;
    }

    public function destroy() {
        /*foreach ($_SESSION as $var => $val) {
            unset($_SESSION[$var]);
        }*/
        session_unset();
        setcookie("PHPSESSID", "", time()-3600);
        session_destroy();
    }

    public function __clone() {
        trigger_error('Clone is not allowed for ' . __CLASS__, E_USER_ERROR);
    }

    public function __get($var) {
        session_regenerate_id();
        return $_SESSION[$var];
    }

    public function __set($var, $val) {
        session_regenerate_id();
        return ($_SESSION[$var] = $val);
    }

    public function __destruct() {
        session_write_close();
    }

}
